from kivy.lang import Builder
from kivy.properties import (ObjectProperty, NumericProperty,
                             StringProperty, ReferenceListProperty,
                             ListProperty)
from kivy.uix.widget import Widget
from os.path import dirname, join
from kivy.uix.image import Image

from kivyrt.utils import dotdict


_gradients_path = join(dirname(__file__), 'textures')


kvstring = '''
<TextureBehavior>
    canvas.after:
        Color
            rgba: self.texture_color
        Rectangle
            pos: self.texture_pos
            size: self.texture_size
            texture: self._texture
'''

texture_names = 'tb bt lr rl s_tb s_bt'.split()

textures = {texture_name: Image(source='{}/{}.png'.format(_gradients_path, texture_name)).texture
            for texture_name in texture_names}


class TextureBehavior(Widget):
    texture = StringProperty()
    _texture = ObjectProperty()
    texture_color = ListProperty([0, 0, 0, 0])

    def on_texture(self, *args):
        self._texture = textures[self.texture]

    _texture_x = NumericProperty()
    _texture_y = NumericProperty()
    texture_pos = ReferenceListProperty(_texture_x, _texture_y)

    _texture_width = NumericProperty()
    _texture_height = NumericProperty()
    texture_size = ReferenceListProperty(_texture_width, _texture_height)


Builder.load_string(kvstring)

