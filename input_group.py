from kivy.lang import Builder
from kivy import properties as kp
from kivy.uix.widget import Widget


_kvstring = '''
<InputGroup>
    size: 0, 0
    size_hint: None, None
'''


class InputGroup(Widget):
    data = kp.DictProperty()
    processed = kp.DictProperty()
    callback = kp.ObjectProperty()
    ready = kp.BooleanProperty(False)
    get_widget_data = kp.ObjectProperty()
    defaults = {}

    def __init__(self, **kwargs):
        super(InputGroup, self).__init__(**kwargs)
        self.widget_data = self.defaults
        self._update_func()

    def _on_data_change(self, data, defaults):
        return {}, defaults

    def _on_activate(self, data, defaults):
        return {}, defaults

    def on_data(self, *args):
        self.processed, self.widget_data = self._on_data_change(self.data, self.defaults.copy())
        self._update_func()

    def _update_func(self):
        self.get_widget_data = lambda name: self.widget_data[name]

    def activate(self):
        self.widget_data = self._on_activate(self.data, self.defaults.copy())
        self._update_func()
        if self.ready:
            self.callback(processed)


def InputBehavior(InterfaceBehavior):
    update_func = ObjectProperty()
    input_name = kp.StringProperty()

    def on_update_func(self, *args):
        self._update_data(self.update_func(self.input_name))


Builder.load_string(_kvstring)

