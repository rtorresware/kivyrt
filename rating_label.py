from itertools import chain

from kivy.properties import NumericProperty, ListProperty
from kivy.uix.label import Label
from kivy.uix.slider import Slider
from kivy.lang import Builder

from kivymd.icon_definitions import md_icons


Builder.load_string('''
<RatingLabel>
    rating: 0
    font_name: 'Icons'

<-RatingSlider>
    color: app.theme_cls.primary_color
    font_size: sp(20)
    pos_hint: {'center_x': .5}
    step: 1
    range: self.step, 5
    value: 0
    width: lb_rating.width
    size_hint_x: None
    RatingLabel
        color: (.6, .6, .6, 1) if not root.value else root.color
        id: lb_rating
        pos: root.pos
        size_hint: None, None
        size: self.texture_size[0], root.height
        rating: root.value
        font_size: root.font_size
        halign: 'center'
        valign: 'center'
''')

star = md_icons['star']
half_star = md_icons['star-half']
empty_star = md_icons['star-outline']


def rating_to_stars(rating):
    num_complete = int(rating)
    half = rating != int(rating)
    num_empty = int(5 - num_complete - 1 * half)
    stars = ''.join(chain((star for _ in range(num_complete)),
                            (half_star if half else '',),
                            (empty_star for _ in range(num_empty))))
    return stars


class RatingSlider(Slider):
    color = ListProperty()


class RatingLabel(Label):
    rating = NumericProperty(6)

    def on_rating(self, *_):
        self.text = rating_to_stars(self.rating)


if __name__ == '__main__':
    from kivy.base import runTouchApp
    import kivymd.theming

    runTouchApp(RatingLabel(rating=4.0))
