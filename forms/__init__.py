from kivy import properties as kp
from kivy.event import EventDispatcher


# States
OK = 0
INVALID = -1
WAITING = 1

OPTIONS = [OK, INVALID, WAITING]


class Field(EventDispatcher):
    default = kp.ObjectProperty()

    """
    Takes a dictionary in the form {function: message_id}
    """
    verifiers = kp.DictProperty()

    _message_id = kp.StringProperty()
    message_id = kp.AliasProperty(lambda x: x._message_id, bind=['_message_id'])

    _state = kp.OptionProperty(INVALID, options=OPTIONS)
    state = kp.AliasProperty(lambda x: x._state, bind=['_state'])

    _value = kp.ObjectProperty()
    value = kp.AliasProperty(lambda x: x._value, bind=['_value'])

    raw_input = kp.ObjectProperty(allownone=True)

    def validate(self):
        for func, msg_id in self.verifiers:

