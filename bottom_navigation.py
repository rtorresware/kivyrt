from kivy.lang import Builder
from kivy.animation import Animation
from kivy import properties as kp

from kivyrt.texturebehavior import TextureBehavior
from kivyrt.subclasses import BgBoxLayout, ButtonBoxLayout
from kivyrt.dataview import DataBehavior
from kivyrt.colors import colors

from collections import namedtuple


_animation_time = .15


Builder.load_string('''
#:import colors kivyrt.colors.colors
#:import md_icons kivymd.icon_definitions.md_icons


<NavBar>
	spacing: dp(4)
	bgcolor: colors.white
	texture_color: [.5, .5, .5, .2]
	texture: 's_bt'
	texture_pos: self.x, self.top
	texture_size: self.width, dp(2)
    viewclass: 'NavButton'

<NavButton>
	app_color: app.theme_cls.primary_color
	isactive: (app.route).split('/')[0] == self.route
	orientation: 'vertical'
	on_release: app.route = self.set_route
	Label
		font_name: 'Icons'
		color: root.color
		font_size: sp(24) + sp(4) * root._alpha
		text: md_icons.get(root.icon, '')
		halign: 'center'
	Label
		color: root.color
		size_hint_y: None
		height: sp(20)
		font_size: sp(12) + sp(2) * root._alpha
		text: root.text
		halign: 'center'
''')


class NavBar(TextureBehavior, DataBehavior, BgBoxLayout):
    pass


class NavButton(ButtonBoxLayout):
    route = kp.StringProperty()
    text = kp.StringProperty()
    set_route = kp.StringProperty()

    icon = kp.StringProperty()

    isactive = kp.BooleanProperty()
    _alpha = kp.NumericProperty(1)

    app_color = kp.ListProperty([0, 0, 0, 0])
    inactive_color = kp.ListProperty(colors.grey500)
    color = kp.ListProperty([0, 0, 0, 0])

    def on_isactive(self, _, isactive):
        alpha = 1 * isactive
        color = self.app_color if isactive else self.inactive_color
        Animation(_alpha=alpha, color=color, d=_animation_time).start(self)

