from kivy.lang import Builder
from kivy import properties as kp
from kivy.uix.screenmanager import Screen
from kivy.utils import get_hex_from_color
from kivy.clock import Clock

from kivymd.theming import ThemableBehavior
from kivymd.icon_definitions import md_icons

from kivyrt.subclasses import BgBoxLayout, ButtonBoxLayout
from kivyrt.mdsubclasses import RectangularButtonBoxLayout, IconLabel


dot = md_icons['record']


kvstring = '''
#:import colors kivyrt.colors.colors
#:import screenmanager kivy.uix.screenmanager

<Stepper>
    num_steps: len(sm.screens)
    orientation: 'vertical'
    _inactive_color: colors.grey500
    _active_color: self.theme_cls.primary_color
    bgcolor: self.theme_cls.bg_normal
    font_size: '16sp'
    #HeightlessBox
        #padding: dp(12)
        #spacing: dp(4)
        #bgcolor: colors.grey200
        #GVMDLabel
            #font_style: 'Subhead'
            #text: (root.step_string).format(root.current_step + 1, root.num_steps)
            #color: self.theme_cls.primary_color
        #GVMDLabel
            #font_style: 'Subhead'
            #text: root.current_title
    ScreenManager
        id: sm
        transition: screenmanager.SlideTransition()
        canvas.before:
            Color
                rgba: colors.grey400
            Rectangle
                pos: self.pos
                size: self.size
    BgBoxLayout
        size_hint_y: None
        height: root.font_size * 3.2
        bgcolor: colors.grey200
        RectangularButtonBoxLayout
            on_release: root.previous()
            disabled: root.current_step == 0
            size_hint_x: None
            width: self.minimum_width
            padding: dp(12)
            IconLabel
                icon: 'chevron-left'
                font_name: 'Icons'
                color: root._active_color
                font_size: root.font_size * 1.4
                disabled_color: root._inactive_color
                width: sp(24)
                size_hint_x: None
            GHMDLabel
                text: root.previous_text
                valign: 'center'
                color: root._active_color
                bold: True
                font_size: root.font_size
                disabled_color: root._inactive_color
        Label
            markup: True
            font_name: 'Icons'
            text: root._dotstring
        RectangularButtonBoxLayout
            on_release: root.next()
            disabled: root.current_step == root.num_steps - 1
            size_hint_x: None
            width: self.minimum_width
            padding: dp(12)
            GHMDLabel
                valign: 'center'
                text: root.next_text
                color: root._active_color
                bold: True
                font_size: root.font_size
                disabled_color: root._inactive_color
            IconLabel
                icon: 'chevron-right'
                font_name: 'Icons'
                color: root._active_color
                font_size: root.font_size * 1.4
                disabled_color: root._inactive_color
                size_hint_x: None
                width: sp(24)

<TestStepper_@Stepper>
    Step
    Step
    Step

<LabeledIconButton>
'''

Builder.load_string(kvstring)


class LabeledIconButton(RectangularButtonBoxLayout):
    icon = kp.StringProperty()
    text = kp.StringProperty()
    color = kp.ListProperty([0, 0, 0, 0])


class Step(Screen):
    index = kp.NumericProperty()
    title = kp.StringProperty()


class Stepper(BgBoxLayout, ThemableBehavior):
    num_steps = kp.NumericProperty()
    current_step = kp.NumericProperty(-1)
    current_title = kp.StringProperty()

    font_size = kp.NumericProperty()

    def __init__(self, **kwargs):
        super(Stepper, self).__init__(**kwargs)
        Clock.schedule_once(lambda x: self.next())

    def on_current_step(self, *args):
        try:
            screen = self.sm.screens[self.current_step]
            self.sm.current = screen.name
            self.current_title = screen.title
        except IndexError:
            pass

    def previous(self):
        self.sm.transition.direction = 'right'
        self.current_step = max((0, self.current_step - 1))

    def next(self):
        self.ids.sm.transition.direction = 'left'
        self.current_step = min((self.num_steps, self.current_step + 1))

    next_text = kp.StringProperty('NEXT ')
    previous_text = kp.StringProperty('BACK')

    step_string = kp.StringProperty('Step {} of {}')

    _active_color = kp.ListProperty([0, 0, 0, 0])
    _inactive_color = kp.ListProperty([0, 0, 0, 0])


    def _calc_dotstring(self):
        colors = {self.current_step: get_hex_from_color(self._active_color)}
        divider_color = get_hex_from_color(self._inactive_color)
        return u''.join(u'[color={}]{}[/color]'.format(colors.get(x, divider_color), dot)
                       for x in range(self.num_steps))

    _dotstring = kp.AliasProperty(_calc_dotstring,
                                  bind=['_inactive_color', '_active_color',
                                        'num_steps', 'current_step'])

    @property
    def sm(self):
        return self.ids.sm

    def add_widget(self, widget):
        if isinstance(widget, Step):
            return self.add_step(widget)
        return super(Stepper, self).add_widget(widget)

    def add_step(self, step):
        self.sm.add_widget(step)


if __name__ == '__main__':
    from kivy.base import runTouchApp
    from kivy.factory import Factory
    runTouchApp(Factory.TestStepper_())

