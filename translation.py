from kivy.properties import StringProperty, DictProperty, ObjectProperty, AliasProperty
from kivy.event import EventDispatcher
from kivy.logger import Logger

import json


class Translator(EventDispatcher):
    language = StringProperty()

    def _get_languages(self):
        return list(self._translated_texts)

    languages = AliasProperty(_get_languages, bind=['_translated_texts'])

    _translation_filename = StringProperty()
    _translated_texts = DictProperty()
    translate = ObjectProperty()

    def __init__(self, filename, language=None, **kwargs):
        super(Translator, self).__init__(**kwargs)
        with open(filename) as language_file:
            self._translated_texts = json.load(language_file)

    def on_languages(self, *args):
        self.language = self.languages[0]

    def _update_translation(self):
        self.translate = lambda msgid: self._translate_text(msgid)

    def _translate_text(self, msgid):
        return self._translated_texts[self.language][msgid.encode('utf8')]

    def on__translated_texts(self, *args):
        self._update_translation()

    def next_language(self):
        current_language_index = self.languages.index(self.language)
        try:
            self.language = self.languages[current_language_index + 1]
        except IndexError:
            self.language = self.languages[0]

    def on_language(self, *args):
        Logger.info('Translator: Changed language to {}'.format(self.language))
        self._update_translation()

