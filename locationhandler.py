import random

from kivy.event import EventDispatcher
from kivy.utils import platform
from kivy.logger import Logger
from kivy.properties import ListProperty, BooleanProperty

from plyer import gps


_mobile_platforms = ['android', 'ios']
is_mobile = platform in _mobile_platforms


def _if_mobile(check=True):
    def wrapper(func):
        def inner_wrapper(*args, **kwargs):
            if is_mobile == check:
                return func(*args, **kwargs)
            Logger.debug('Platform: Not calling function {} becuase is_mobile is {}'.format(func, is_mobile))
        return inner_wrapper
    return wrapper


class LocationHandler(EventDispatcher):
    location = ListProperty([20.669, -103.416])
    _first_update_done = BooleanProperty(False)

    def __init__(self, random_ratio=.1, **kwargs):
        super(LocationHandler, self).__init__(**kwargs)
        self._default_kwargs = {'on_location': self._set_location}
        self.random_ratio = random_ratio
        self.update()

    def on_location(self, *args):
        self._first_update_done = True
        Logger.info('Location: updated to {}'.format(self.location))

    def _set_location(self, **kwargs):
        self.location = kwargs['lat'], kwargs['lon']

    @_if_mobile(True)
    def start(self):
        Logger.info('Location: turning location service ON')
        self.gps.start()

    @_if_mobile(True)
    def stop(self):
        Logger.info('Location: turning location service OFF')
        self.gps.stop()

    @property
    def _random(self):
        return (random.random() - .5) * self.random_ratio

    @_if_mobile(False)
    def _randomize_location(self):
        Logger.info('Location: mocking new location...')
        old_location = self.location
        while self.location == old_location:
            self.location = [dim + self._random for dim in old_location]

    def on__first_update_done(self, *args):
        if self._first_update_done:
            self.stop()
        self.start()

    def update(self):
        self._first_update_done = False
        self._randomize_location()

    @_if_mobile(True)
    def configure(self, **kwargs):
        new_kwargs = self._default_kwargs.copy().update(kwargs)
        gps.configure(**new_kwargs)

