from kivy import properties as kp


class View(object):
    view_cls = None
    vm = kp.ObjectProperty(rebind=True)

    def __init__(self, *args, **kwargs):
        self.vm = self.view_cls()
        super(View, self).__init__(*args, **kwargs)

