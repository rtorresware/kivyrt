from kivy.graphics import InstructionGroup, Color, Rectangle
from kivy.properties import (ListProperty, ObjectProperty, StringProperty,
                             NumericProperty, OptionProperty, DictProperty)
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.widget import Widget
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.uix.recycleview import RecycleView
from kivy.uix.carousel import Carousel

from kivy.lang import Builder


Builder.load_string('''
<HeightlessGrid>:
    size_hint_y: None
    height: self.minimum_height
    bgcolor: 0, 0, 0, 0
    canvas.before:
        Color:
            rgba: self.bgcolor
        Rectangle:
            pos: self.pos
            size: self.size

<HeightlessBox>
    height: self.minimum_height
    size_hint_y: None
    orientation: 'vertical'

<BgBehavior>
    bgcolor: 0, 0, 0, 0
    canvas.before:
        Color:
            rgba: self.bgcolor
        RoundedRectangle:
            pos: self.pos
            size: self.size
            radius: self.bg_radius

<ShadeBehavior>
    canvas.after:
        Color:
            rgba: 0, 0, 0, .5
        Rectangle:
            pos: self.x, self.y - self.shade_length
            size: self.width, self.shade_length
            # texture: texture_tb

<NoFloatLayout@FloatLayout>
    size: 0, 0
    size_hint: None, None

<GrowVerticalLabel>:
    height: self.texture_size[1] * self._height_ratio
    text_size: self.width, None
    size_hint_y: None
    valign: 'middle'
    halign: 'left'

<Separator>
    size_hint_y: None
    height: dp(1)

<GHLabel>:
    size_hint_x: None
    width: self.texture_size[0]
    text_size: None, self.height
    valign: 'middle'
    halign: 'center'

<GrowVerticalButton>:
    size: self.texture_size[0], self.texture_size[1] * 1.2
    text_size: self.width, None
    size_hint_y: None
    halign: 'center'
    valign: 'middle'

<CompactLabel>:
    size: self.texture_size
    size_hint: None, None
    halign: 'center'
    bgcolor: 0, 0, 0, 0
    valign: 'middle'
    font_size: 25
    color: 0, 0, 0, 1
''')


class BgBehavior(object):
    bg_radius = ListProperty([0, 0, 0, 0])
    bgcolor = ListProperty()


class ShadeBehavior(object):
    shade_length = NumericProperty(0)


class BgBoxLayout(BgBehavior, BoxLayout):
    pass


class ButtonBoxLayout(ButtonBehavior, BgBoxLayout):
    pass


class BgGridLayout(GridLayout, BgBehavior):
    pass


class HeightlessGrid(BgGridLayout):
    pass


class HeightlessBox(BgBoxLayout):
    pass


class ImageButton(ButtonBehavior, Image):
    pass


class GrowVerticalLabel(Label):
    _height_ratio = NumericProperty(1)


class GHLabel(Label):
    pass


class GrowVerticalButton(Button):
    pass


class CompactLabel(Label):
    pass


class Separator(BgBehavior, Widget):
    pass


def merge_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z


class ExtraRecycleView(RecycleView):
    _extra_kwargs = DictProperty()
    pre_data = ListProperty()

    def on_pre_data(self, *args):
        self.data = [merge_dicts(data, self._extra_kwargs) for data in self.pre_data]


class TextureBox(BoxLayout):
    bgcolor = ListProperty([0, 0, 0, 0])
    texture = ObjectProperty()
    source = StringProperty()
    _texture_size = ListProperty([0, 0])
    texture_color = ListProperty([0, 0, 0, 0])
    texture_ratio = NumericProperty(1)

    def on_source(self, _, s):
        imagen = Image(source=s)
        self._texture_size = [x * self.texture_ratio for x in imagen.texture_size]
        texture = imagen.texture
        texture.wrap = 'repeat'
        self.texture = texture
        self.on_size()

    def on_texture_color(self, *_):
        self.on_size()

    def on_size(self, *_):
        if self.texture:
            self.canvas.before.clear()
            grupo = InstructionGroup()
            grupo.add(Color(*self.bgcolor))
            grupo.add(Rectangle(pos=self.pos, size=self.size))
            grupo.add(Color(*self.texture_color))
            try:
                uvsize = self.width / self._texture_size[0], self.height / self._texture_size[1]
            except ZeroDivisionError:
                uvsize = 1, 1
            self.texture.uvsize = uvsize
            group.add(Rectangle(pos=self.pos, size=self.size, texture=self.texture))
            self.canvas.before.add(grupo)

