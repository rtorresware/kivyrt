from kivy.storage.jsonstore import JsonStore
from kivy.logger import Logger
import json
from ..binder import propbinder


class AutosaveStore(JsonStore):
    def __init__(self, filename, instructions, **kwargs):
        try:
            super(AutosaveStore, self).__init__(filename, **kwargs)
        except ValueError:
            json.dump({}, open(filename, 'w'))
            super(AutosaveStore, self).__init__(filename, **kwargs)
        for whose, propnames in instructions.items():
            self.load_and_autosave(propnames, whose)

    def autosave_properties(self, propnames, whose):
        Logger.info('Autosave: Persisting the following properties: {} into {}'.format(', '.join(propnames),
                                                                                       whose.__class__.__name__))
        propbinder(propnames, whose, self._save_property)

    def load_properties(self, propnames, into):
        for propname in propnames:
            try:
                setattr(into, propname, self.get(propname)['value'])
            except:
                Logger.warning('Autosave: Property {} has not yet been saved'.format(propname))

    def load_and_autosave(self, propnames, target):
        self.load_properties(propnames, target)
        self.autosave_properties(propnames, target)

    def _save_property(self, prop, value):
        Logger.info('Autosave: saving property {}'.format(prop))
        self.put(prop, value=value)

