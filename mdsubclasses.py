from kivy.uix.behaviors import ButtonBehavior
from kivy.properties import StringProperty
from kivy.lang import Builder
from kivy.uix.label import Label

from kivymd.label import MDLabel
from kivymd.icon_definitions import md_icons
from kivymd.theming import ThemableBehavior
from kivymd.ripplebehavior import CircularRippleBehavior, RectangularRippleBehavior


from kivyrt.subclasses import CompactLabel, GrowVerticalLabel, GHLabel, ButtonBoxLayout


class CompactMDLabel(CompactLabel, MDLabel):
    pass


class GVMDLabel(GrowVerticalLabel, MDLabel):
    pass


class GHMDLabel(GHLabel, MDLabel):
    pass


class IconLabel(ThemableBehavior, Label):
    icon = StringProperty()

    def on_icon(self, *_):
        self.text = md_icons[self.icon]


class IconButton(CircularRippleBehavior, ButtonBehavior, IconLabel):
    pass


class CircularButtonBoxLayout(CircularRippleBehavior, ButtonBoxLayout):
    pass


class RectangularButtonBoxLayout(RectangularRippleBehavior, ButtonBoxLayout):
    pass


Builder.load_string('''
<IconLabel>
    font_name: 'Icons'

<IconButton>
    font_name: 'Icons'
    font_size: sp(20)
''')
