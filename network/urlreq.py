from kivy import Logger
from kivy.network.urlrequest import UrlRequest
from kivy.weakmethod import WeakMethod

from json import dumps


class SUrlRequest(UrlRequest):
    def __init__(self, url, json=None, req_headers=None, req_body=None,
                 on_finished=None, **kwargs):
        req_headers = req_headers or {}
        self._json = json
        if json:
            req_headers['Content-Type'] = 'application/json'
            req_body = dumps(json)
        self.on_finished = WeakMethod(on_finished) if on_finished else None
        super(SUrlRequest, self).__init__(url, req_headers=req_headers, req_body=req_body, **kwargs)

    def _dispatch_result(self, dt):
        while True:
            # Read the result pushed on the queue, and dispatch to the client
            try:
                result, resp, data = self._queue.pop()
            except IndexError:
                return
            if resp:
                # XXX usage of dict can be dangerous if multiple headers
                # are set even if it's invalid. But it look like it's ok
                # ?  http://stackoverflow.com/questions/2454494/..
                # ..urllib2-multiple-set-cookie-headers-in-response
                self._resp_headers = dict(resp.getheaders())
                self._resp_status = resp.status
            if result == 'success':
                status_class = resp.status // 100

                if status_class in (1, 2):
                    if self._debug:
                        Logger.debug('UrlRequest: {0} Download finished with'
                                     ' {1} datalen'.format(id(self),
                                                           len(data)))
                    self._is_finished = True
                    self._result = data
                    if self.on_success:
                        func = self.on_success()
                        if func:
                            func(self, data)

                elif status_class == 3:
                    if self._debug:
                        Logger.debug('UrlRequest: {} Download '
                                     'redirected'.format(id(self)))
                    self._is_finished = True
                    self._result = data
                    if self.on_redirect:
                        func = self.on_redirect()
                        if func:
                            func(self, data)

                elif status_class in (4, 5):
                    if self._debug:
                        Logger.debug('UrlRequest: {} Download failed with '
                                     'http error {}'.format(id(self),
                                                            resp.status))
                    self._is_finished = True
                    self._result = data
                    if self.on_failure:
                        func = self.on_failure()
                        if func:
                            func(self, data)

            elif result == 'error':
                if self._debug:
                    Logger.debug('UrlRequest: {0} Download error '
                                 '<{1}>'.format(id(self), data))
                self._is_finished = True
                self._error = data
                if self.on_error:
                    func = self.on_error()
                    if func:
                        func(self, data)

            elif result == 'progress':
                if self._debug:
                    Logger.debug('UrlRequest: {0} Download progress '
                                 '{1}'.format(id(self), data))
                if self.on_progress:
                    func = self.on_progress()
                    if func:
                        func(self, data[0], data[1])
            else:
                assert(0)

            if self._is_finished and self.on_finished:
                func = self.on_finished()
                if func:
                    func(self, data)

