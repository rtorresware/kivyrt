import json

from .urlreq import SUrlRequest

try:
    from urllib import urlencode
except:
    from urllib.parse import urlencode


from kivy.event import EventDispatcher
from kivy.logger import Logger
from kivy.properties import DictProperty


_url_keys = 'key params'.split()


def _join_callbacks(*callbacks):
    def call_all(*args, **kwargs):
        return [callback(*args, **kwargs) for callback in callbacks if
                callback is not None]
    return call_all


def _pass_args(function):
    return lambda *args, **kwargs: function(*args, **kwargs)


def _with_callback_to(function, name='on_success', processor=_pass_args):
    def wrapper(func):
        def add_callback(instance, *args, **kwargs):
            callback = lambda req, res: function(instance, req, res)
            kwargs[name] = _join_callbacks(processor(callback),
                                            kwargs.get(name, None))
            return func(instance, *args, **kwargs)
        return add_callback
    return wrapper


def _get_processor(callback):
    return lambda req, res: callback(req, res['results'])


def _delete_from_cache(instance, req, res):
    instance.cache = {key: value for key, value in instance.cache.items()
                      if key not in res}


def _update_cache(instance, req, res):
    instance.cache.update(res)


def _set_query_results(instance, req, res):
    instance.query_results = res


def _clear_query_results(instance, req, res):
    instance.query_results.clear()


def _set_cache(instance, req, res):
    instance.cache = res


def _clear_cache(instance, req, res):
    instance.cache.clear()
    instance.headers.clear()


def _set_cookie(instance, req, res):
    #TODO get cookie according to dev or production
    instance.headers['cookie'] = req._resp_headers['set-cookie']


class DatastoreBaseModel(EventDispatcher):
    cache = DictProperty()

    def __init__(self, configurator, model_name, notifier=None, **kwargs):
        super(DatastoreBaseModel, self).__init__()
        self.configurator = configurator
        self.model_name = model_name
        self.notifier = notifier

    def mock(self, filename):
        with open(filename) as jsonfile:
            self.cache = json.load(jsonfile)

    @property
    def endpoint(self):
        return '/'.join((self.configurator.apiurl, self.model_name))

    def _suffixfor(self, key=None, params=None):
        if key is not None:
            return '/{}'.format(key)
        if params is not None:
            return '?{}'.format(urlencode(params))
        return ''

    def _urlfor(self, **url_kwargs):
        return '{}{}'.format(self.endpoint, self._suffixfor(**url_kwargs))

    def _with_headers(func):
        def add_headers(self, *args, **kwargs):
            new_kwargs = {'req_headers': self.configurator.headers.copy()}
            new_kwargs.update(self.configurator.extra_kwargs)
            new_kwargs.update(kwargs)
            return func(self, *args, **new_kwargs)
        return add_headers

    @staticmethod
    def _build_key_query(propname, key):
        '''key query must have single quotations surrounding key'''
        return "{}=KEY('{}')".format(propname, key)

    def _notify(self, req, res):
        if self.notifier:
            self.notifier(req, res)

    @_with_headers
    @_with_callback_to(_notify, 'on_failure')
    @_with_callback_to(_notify, 'on_error')
    def _make_request(self, **kwargs):
        url_kwargs = {key: kwargs.pop(key) for key in _url_keys if key in kwargs}
        return SUrlRequest(self._urlfor(**url_kwargs), **kwargs)

    def getmine(*args, **kwargs):
        raise NotImplementedError

    def get(*args, **kwargs):
        raise NotImplementedError

    def post(*args, **kwargs):
        raise NotImplementedError

    def put(*args, **kwargs):
        raise NotImplementedError

    def delete(*args, **kwargs):
        raise NotImplementedError


class ConfiguratorModel(DatastoreBaseModel):
    headers = DictProperty()

    def __init__(self, apiurl, model_name='users', **kwargs):
        self.apiurl = apiurl
        self.key = 'me'
        self.extra_kwargs = kwargs
        super(ConfiguratorModel, self).__init__(self, model_name, **kwargs)

    @property
    def user_key(self):
        try:
            return self.cache.get('id')
        except KeyError:
            Logger.warning('Datastore: no key in user, request will fail')
            return ''

    def get_mine_query(self, owner_propname='owner'):
        return self._build_key_query(owner_propname, self.user_key)

    def clear(self, *args):
        self.headers.clear()
        self.cache.clear()

    @_with_callback_to(_set_cache)
    @_with_callback_to(_set_cookie)
    def login(self, data, **kwargs):
        return self._make_request(key='login', method='POST', json=data, **kwargs)

    @_with_callback_to(_set_cache)
    def get(self, **kwargs):
        return self._make_request(key=self.key, method='GET', **kwargs)

    def post(self, data, **kwargs):
        return self._make_request(method='POST', json=data, **kwargs)

    @_with_callback_to(_set_cache)
    def put(self, data, **kwargs):
        return self._make_request(key=self.key, method='PUT', json=data, **kwargs)

    @_with_callback_to(_clear_cache)
    def delete(self, **kwargs):
        return self._make_request(key=self.key, method='DELETE', **kwargs)


class DatastoreModel(DatastoreBaseModel):
    query_results = DictProperty()

    @_with_callback_to(_update_cache)
    @_with_callback_to(_set_query_results)
    @_with_callback_to(_clear_query_results, name='on_failure')
    def query(self, fulltext=None, **kwargs):
        return self._make_request(fulltext=fulltext, method='GET', **kwargs)

    @_with_callback_to(_update_cache, processor=_get_processor)
    @_with_callback_to(_set_query_results, processor=_get_processor)
    @_with_callback_to(_clear_query_results, name='on_failure')
    def key_query(self, propname, key, **kwargs):
        query = self._build_key_query(propname, key)
        return self._make_request(query=query, method='GET', **kwargs)

    @_with_callback_to(_update_cache, processor=_get_processor)
    def get(self, key=None, **kwargs):
        return self._make_request(key=key, method='GET', **kwargs)

    @_with_callback_to(_update_cache)
    def post(self, data, **kwargs):
        return self._make_request(method='POST', json=data, **kwargs)

    @_with_callback_to(_update_cache)
    def put(self, data, key=None, **kwargs):
        return self._make_request(key=key, method='PUT', json=data, **kwargs)

    @_with_callback_to(_delete_from_cache)
    def delete(self, key, **kwargs):
        return self._make_request(key=key, method='DELETE', **kwargs)


class OwnedDatastoreModel(DatastoreModel):
    mine = DictProperty()

    def __init__(self, configurator, model_name, owner_propname='owner', **kwargs):
        super(OwnedDatastoreModel, self).__init__(configurator, model_name, **kwargs)
        self.owner_propname = owner_propname
        self.bind(cache=self._set_mine)
        self.configurator.bind(cache=self._set_mine)

    def _set_mine(self, *args):
        self.mine = {key: value for key, value in self.cache.items()
                     if value[self.owner_propname] == self.configurator.user_key}

    @_with_callback_to(_update_cache, processor=_get_processor)
    def getmine(self, **kwargs):
        query = self.configurator.get_mine_query(self.owner_propname)
        return self._make_request(query=query, method='GET', **kwargs)

