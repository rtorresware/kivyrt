import json
from kivyrt.paths import abspathfor
from kivyrt.utils import dotdict
from kivy.utils import get_color_from_hex


with open(abspathfor('material_colors.json', __file__)) as file_:
    _colors_hex = json.load(file_, object_pairs_hook=dotdict)
    colors = dotdict({color: get_color_from_hex(hex_)
                      for color, hex_ in _colors_hex.items()})

if __name__ == '__main__':
    from kivy.app import App
    from kivy.lang import Builder
    from kivy.properties import DictProperty

    color_string = '''
BoxLayout
    orientation: 'vertical'
    TextInput
        id: search
        size_hint_y: .1
    RecycleView
        data:
            [{'bgcolor': app.colors[key], 'name': key}
            for key in sorted(app.colors)
            if search.text in key]
        viewclass: 'ColorWidget'
        RecycleGridLayout
            cols: 3
            height: self.minimum_height
            size_hint_y: None
            default_size_hint: 1, None
            default_size: None, dp(60)

<ColorWidget@BoxLayout>
    bgcolor: 0, 0, 0, 0
    color: 0, 0, 0, 1
    name: ''
    Label
        text: root.name
    Widget
        canvas:
            Color
                rgba: root.bgcolor
            Rectangle
                pos: self.pos
                size: self.size
'''

    class ColorApp(App):
        colors = DictProperty(colors)

        def build(self):
            return Builder.load_string(color_string)

    ColorApp().run()

