from kivy.core.text import LabelBase

try:
    from kivy.lang.builder import Builder
except ImportError:
    from kivy.lang import Builder


def change_loader_image(filename, **kwargs):
    return 
    kwargs = kwargs or {}
    from kivy.loader import Loader
    from kivy.core.image import Image as CoreImage
    image = CoreImage(absfilename(filename), **kwargs)
    image.anim_delay = 1 / 160.
    image.scale = .5
    Loader.loading_image = image


def register_fonts(fonts):
    for name, fontsname in fonts:
        LabelBase.register(name, fontsname)
    return


def load_kvs(kvfiles):
    for kvfile in kvfiles:
        Builder.load_file(kvfile)
    return

