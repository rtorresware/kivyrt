from kivy.properties import DictProperty, ObjectProperty


def update_data(widget, data):
    [setattr(widget, propname, value) for propname, value in data.items()]


class InterfaceBehavior(object):
    data = DictProperty()

    def on_data(self, *args):
        update_data(self, self.data)

