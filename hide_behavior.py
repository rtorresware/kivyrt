from kivy.animation import Animation
from kivy.effects.dampedscroll import DampedScrollEffect
from kivy.properties import ObjectProperty, NumericProperty, BooleanProperty
from kivy.clock import Clock

from kivyrt.subclasses import BgBoxLayout


class DragEffect(DampedScrollEffect):
    blocked = True

    def on_value(self, *args):
        if self.blocked:
            return True
        return super(DragEffect, self).on_value(*args)


class HideBox(BgBoxLayout):
    height_alpha = NumericProperty(0)
    scroll = ObjectProperty()

    _anim_duration = NumericProperty(.15)

    def on_scroll(self, *_):
        self.scroll.effect_cls = DragEffect

    def on_touch_down(self, touch):
        if self.height_alpha in [0, 1]:
            touch.allowed = True
        return super(HideBox, self).on_touch_down(touch)

    def on_touch_move(self, touch):
        DragEffect.blocked = True
        if not hasattr(touch, 'allowed'):
            return True
        if self.height_alpha < 1 or (touch.osy > touch.sy and self.scroll.scroll_y >= 1):
            self._reset_scroll()
            self.height_alpha = sorted((0, self.height_alpha + touch.dsy, 1))[1]
            return True
        DragEffect.blocked = False
        return super(HideBox, self).on_touch_move(touch)

    def _animate_state(self, target):
        Animation(height_alpha=target, d=self._anim_duration).start(self)

    def _reset_scroll(self, *args):
        self.scroll.scroll_y = 1

    def on_touch_up(self, touch):
        if self.height_alpha > .8:
            self.open()
        else:
            self.close()
        return super(HideBox, self).on_touch_up(touch)

    def close(self):
        self._animate_state(0)

    def open(self):
        self._animate_state(1)

