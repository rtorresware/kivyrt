import webbrowser


def open_link(link):
    print(link)
    if link:
        try:
            int(link[0])
            return webbrowser.open('tel: {}'.format(link))
        except:
            if link.startswith('http'):
                return webbrowser.open(link)
            return webbrowser.open('https://{}'.format(link))

