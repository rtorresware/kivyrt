def _make_binder(propname, func):
    return lambda _, value: func(propname, value)

def propbinder(propnames, whose, func):
    kwargs = {propname: _make_binder(propname, func)
              for propname in propnames}
    whose.bind(**kwargs)

