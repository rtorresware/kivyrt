import numbers
from collections import defaultdict
from datetime import datetime


class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    def __getattr__(self, attr):
        return self.get(attr, '')
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__


def timestamp_tostring(timestamp):
    try:
        fecha = datetime.fromtimestamp(timestamp)
        return fecha.strftime('%d/%m/%Y')
    except:
        return str(timestamp)


def cleanse_text(text):
    return ' '.join(text.lower().split())


class keydefaultdict(defaultdict):
    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        else:
            ret = self[key] = self.default_factory(key)
            return ret


def cumple_con_filtro(entidad, d_filtros):
    for nombre, lista in d_filtros.items():
        valor_entidad = entidad[nombre]
        if lista and not valor_entidad:
            return False
        if isinstance(valor_entidad, numbers.Number):
            meses = [f for f in lista if f.tipo == 'mes']
            lista = [f for f in lista if f.tipo != 'mes']
            if any(f.contenido <= valor_entidad for f in lista if f.operador == '<'):
                return False
            if any(f.contenido >= valor_entidad for f in lista if f.operador == '>'):
                return False
            if any(f.contenido == valor_entidad for f in lista if f.operador in '<> !='.split()):
                return False
            if any(f.contenido != valor_entidad for f in lista if f.operador is None):
                return False
            if any(f.contenido != datetime.fromtimestamp(valor_entidad).month for f in meses):
                return False
        elif isinstance(valor_entidad, str):
            valor_entidad = valor_entidad.lower()
            if any(f.contenido in valor_entidad for f in lista if f.contenido and f.operador == '-'):
                # print('-', False)
                return False
            if any(f.operador is None for f in lista):
                if not any(f.contenido in valor_entidad for f in lista if f.operador is None):
                    # print(None, False)
                    return False
            if not all(f.contenido in valor_entidad for f in lista if f.operador == '+'):
                # print('+', False)
                return False
    return True


def getitems(dictionary):
    return dictionary.items()

