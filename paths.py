from os.path import dirname, join
from kivy.lang import Builder


_kvfiles = []

def abspathfor(filename, reference_filename):
    return join(dirname(reference_filename), filename)


def load_kv(kvfilename, reference_filename):
    filename = abspathfor(kvfilename, reference_filename)
    _kvfiles.append(filename)
    Builder.load_file(filename)


def unload_kvfiles(kvfiles=_kvfiles):
    [Builder.unload_file(kvfile) for kvfile in kvfiles]

