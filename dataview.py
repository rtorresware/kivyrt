from collections import defaultdict
from itertools import chain

from kivy.factory import Factory
from kivy.uix.behaviors import ButtonBehavior
from kivy.properties import (ListProperty, StringProperty, DictProperty,
                             ObjectProperty, BooleanProperty)
from kivy.event import EventDispatcher

from kivyrt.subclasses import BgBoxLayout


class DataBehavior(EventDispatcher):
    data = ListProperty()
    viewclass = StringProperty('')
    default_size = ListProperty([None, None])
    default_size_hint = ListProperty([1, 1])
    _extra_kwargs = DictProperty()

    def __init__(self, **kwargs):
        super(DataBehavior, self).__init__(**kwargs)
        self._views = defaultdict(self._create_view)
        self.bind(default_size=self._set_default_kwargs)
        self.bind(default_size_hint=self._set_default_kwargs)
        self.bind(_extra_kwargs=self._recalc_views)
        self.bind(data=self._recalc_views)
        self.bind(viewclass=self._recalc_views)

    def _recalc_views(self, *args):
        if self.viewclass:
            self._clear_views()
            for index, data in enumerate(self.data):
                widget = self._views[index]
                data['index'] = index
                for key, value in chain(data.items(), self._extra_kwargs.items()):
                    setattr(widget, key, value)
                self.add_widget(widget)

    def _clear_views(self):
        [widget.parent.remove_widget(widget)
         for widget in self._views.values() if widget.parent]

    def _create_view(self):
        return Factory.get(self.viewclass)()

    def _set_default_kwargs(self, *_):
        default_kwargs = {name: value for name, value in
                          zip('width height'.split(), self.default_size)
                          if value is not None}
        default_kwargs['size_hint'] = self.default_size_hint
        self._extra_kwargs = default_kwargs

    def on_viewclass(self, *_):
        self._clear_views()
        self._views.clear()
        self._recalc_views()


class SelectionBehavior(ButtonBehavior):
    view = ObjectProperty()
    selected = BooleanProperty(False)

    @property
    def _select_or_deselect(self):
        return self._deselect if self.selected else self._select

    def _select(self, key):
        self.view.selected.append(key)

    def _deselect(self, key):
        try:
            self.view.selected.remove(key)
        except:
            pass


class DataBox(DataBehavior, BgBoxLayout):
    pass

